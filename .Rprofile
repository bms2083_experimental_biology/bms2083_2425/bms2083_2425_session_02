message("Hello!")

cat("Date and time:\n")
format(Sys.time(), "%a %b %e %H:%M:%S %Y")

cat("Print USERNAME:\n")
Sys.getenv("USERNAME")

cat("Print HOME:\n")
Sys.getenv("HOME")

cat("Your current working directory is:\n")
getwd()

cat("Number of installed packages: ",
    nrow(utils::installed.packages()), "\n")

cat("libPaths:\n")
.libPaths()


#===================================================
# Tidyverse packages:
#===================================================
packs.tidyverse <- c(
  "ggplot2", "dplyr", "purrr", "tidyr", "readr", "tibble",
  "stringr", "forcats"
)


#===================================================
# Required packages:
#===================================================
packs.tmp.1 <- c("rstudioapi", "DT", "magrittr", "palmerpenguins",
                 "rmarkdown", "sessioninfo", "yaml", "plotly")

packs.tmp.2 <- setdiff(packs.tmp.1, packs.tidyverse)

packs.shinywidgets.dependencies <- c("anytime", "bslib", "sass", "shiny",
                                     "htmltools", "jsonlite", "rlang", "remotes")

packs.required <- c("tidyverse", packs.tmp.2, packs.shinywidgets.dependencies,
                    "miniUI", "shinyWidgets", "esquisse")

rm(packs.tmp.1, packs.tmp.2, packs.shinywidgets.dependencies)

cat("Number of REQUIRED packages: ",
    length(packs.required), "\n\n", sep="")


#===================================================
# Already installed packages:
#===================================================
packs.installed <- row.names(utils::installed.packages())
cat("Number of already INSTALLED packages: ",
    length(packs.installed), "\n\n", sep="")


#===================================================
# Missing packages:
#===================================================
packs.missing <- setdiff(packs.required, packs.installed)
cat("Number of MISSING packages: ",
    length(packs.missing), "\n\n", sep="")

cat(packs.missing, sep=", ")
cat("\n")


#===================================================
# Binary package versions only:
#===================================================
options(install.packages.check.source = "no")

# Also adding argument: type="binary"


#===================================================
# Update installed packages:
#===================================================
cat("## Update installed packages:\n")

packs.update <- setdiff(packs.installed, packs.required)
cat("Number of installed packages to update: ",
    length(packs.update), "\n\n", sep="")

if (length(packs.missing) > 0) {
  if (length(packs.update) > 0) {
    for (pack.i in packs.update) {
      message(paste("Try to update package:", pack.i), "\n")
      try(
        utils::update.packages(pack.i, repos="https://cran.rstudio.com/",
                               ask=FALSE, verbose = FALSE, type="binary"),
        silent=FALSE
      )
    }
  }
}
if (exists("pack.i")) rm(pack.i)

#===================================================
# Install missing packages:
#===================================================
cat("## Install missing packages:\n")
# In many cases, packages might have been installed as dependencies
# previously. So, before installing a package, check whether it has
# already been installed earlier.
if (length(packs.missing) > 0) {
  for (pack.i in packs.missing) {
    message(paste("Try to install package:", pack.i), "\n")
    installed.packs.updated <- row.names(utils::installed.packages())
    if (pack.i %in% installed.packs.updated) {
      cat(pack.i, "already installed.\n")
    } else {
      try(
        utils::install.packages(pack.i, repos="https://cran.rstudio.com/",
                                ask=FALSE, verbose = FALSE, type="binary")
      ) # end try
    } # end if
  } # end for
} # end if
if (exists("pack.i")) rm(pack.i)

cat("Number of installed packages: ",
    nrow(utils::installed.packages()), "\n")

#===================================================
# Remove variables:
#===================================================
cat("## Remove variables\n")
if (exists("installed.packs.updated")) rm(installed.packs.updated)
if (exists("packs.installed")) rm(packs.installed)
if (exists("packs.missing")) rm(packs.missing)
if (exists("packs.required")) rm(packs.required)
if (exists("packs.tidyverse")) rm(packs.tidyverse)
if (exists("packs.update")) rm(packs.update)


#===================================================
# Load packages:
#===================================================
cat("## Load packages\n")
library(ggplot2)
library(esquisse)
library(magrittr)
library(plotly)


#===================================================
# Session info:
#===================================================
cat("## Session info:\n")
utils::sessionInfo()


message("Done here.\n")

