# BMS2083_2425_Session_02

## Intro to R programming

Axel Nohturfft  
*03/04-Oct-2024*  

## Learning topics  

* Recap of the previous week
* Variables  
* Functions  
* Data types  
* Data structures (vectors, data frames)  
* Subsetting vectors and data frames  
* Getting / finding help  


## To create a copy of this project repository follow these steps:

1. Open RStudio  
2. File Menu > New Project > Version Control > Git  
3. Paste this Gitlab URL into the "Repository URL" field: https://gitlab.surrey.ac.uk/bms2083_experimental_biology/bms2083_2425/bms2083_2425_session_02  
4. Press tab key ("Project directory name" field will be filled automatically)  
5. Choose a suitable folder in the "Create project as subdirectory of..." field  
6. Click the "Create project" button  
7. Wait for setup process to complete  
8. Open the first Rmd script ...  

