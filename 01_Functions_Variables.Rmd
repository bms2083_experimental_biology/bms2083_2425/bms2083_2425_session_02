---
title: "BMS2083: R Workshops (2024/25)"
subtitle: "Functions and variables"
author: "Axel Nohturfft"
date: "`r Sys.Date()`"
output: 
  html_notebook:
    toc: TRUE
    toc_float: TRUE
    toc_depth: 3
    code_folding: "show"
---

## Topics covered in this script  

* Functions  
* Variables  


```{r include=FALSE, echo=FALSE}
# Surrey colors:
# Gold: c4a52e
# Blue: 1f3f79
```


```{css Formatting-html-output, echo=FALSE}
h1 {background: #1f3f79;color: white;padding-left: 7px;}
h2 {background: #c4a52e;color: white;padding-left: 7px;}
h3 {color: #1f3f79;}
code {color: chocolate;}
```



## Functions

### Arithmetic functions  

These are just your standard arithmetic operators, e.g.:

```{r Arithmetic-functions}
100 * 89
100 + 89
100 - 89
100 / 90
10 ^ 4
```


### Named functions

Named functions always have a unique identifier followed by round brackets.
Extra pieces of information added within the brackets are called **arguments**.

_**EXERCISE:**_ Load the irises.csv file saved in the current project and save the code in the chunk below:
```{r Load-iris-data}

```


_**EXERCISE:**_ Using AddIns > 'ggplot2' builder generate a scatterplot from the irises data plotting Sepal.Length (y axis) against Petal.Length (x axis) and save the code in the chunk below:
```{r Exercise-ggplot2}

```



## Variables  
Usually data are stored in named VARIABLES.  

* Variables are defined with the '<-' assignment operator  
* Variable names can be (almost) any length  
* Variables CANNOT contain spaces  
* Variables CANNOT contain dashes/hyphens    
* Variables CANNOT start with a number (but numbers can appear elsewhere in the variable name) 

In the above code chunk, the data from the file 'irises.csv' was stored in the variable 'irises'.

Going forward, we just refer to the variable to analyse or manipulate the data.


## Some examples using named functions:

Display data frame:
```{r Display-data-frame}
print(irises)
```


### Print column headers:
```{r Column-names-print}
# Print column headers:
nam <- names(irises)
print(nam)
```

Generally, we can omit the `print()` function:
```{r Column-headers-no-print}
nam
```


### Print column headers separated by newline characters
Note how we use TWO ARGUMENTS, separated by a comma.
```{r cat-column-names}
cat(nam, sep="\n")
```

## Packages

Packages provide new functions.

Packages first need to be installed on your computer using the `install.packages()` function.

* Use this function only once per session on a university computer.  
* Use this function only once on your own computer, it will be saved permanently.  

Once installed, a package's functions can be loaded using the `library()` function.

Let's say, we want to summarise the irises data using the `describe()` function from the `psych` package:  

First install the `psych` package:  
```{r Install-psych}
install.packages("psych")
```

Then load the package with `library()`:
```{r library-pysch}
# Note how, in this case, we don't have to use quotation marks for the argument.
library(psych)
```


Now we can use a function (`describe()`) from the `psych` package:
```{r describe-irises}
describe(irises)
```


To see a list of all functions provided by a specific packages, use the following code:
```{r package-help}
help(package="psych")
```







